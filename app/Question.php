<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable=['id','title','date','description','user_id','view'];
    public function User()
    {
        return $this->belongsTo(User::class);
    }
    public function settings()
    {
        return $this->hasMany(Setting::class);
    }
}
