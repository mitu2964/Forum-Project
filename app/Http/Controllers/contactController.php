<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class contactController extends Controller
{
    public function index()
    {
        return view('layouts.includes.contact');
    }
    public function contact(Request $request)
    {
        $this->validate($request,[
            'email'=> 'required|email',
            'subject'=>'required|min:3',
            'message'=>'required|min:10',
        ]);
        $data= array(
            'email'=>$request->email,
            'subject'=>$request->subject,
            'bodyMessage'=>$request->message,
        );
        Mail::send('emails.contact',$data,function ($message) use ($data){
//            $message->from($data['name']);
            $message->from($data['email']);
            $message->to('arinaafrin05@gmail.com');
            $message->subject($data['subject']);
        });
        return redirect()->url('/');
    }
}
