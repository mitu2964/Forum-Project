<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WelcomeController extends Controller
{

    public function question()
    {
        $users=DB::table('questions')
            ->join('settings', 'questions.user_id','=','settings.user_id')
            ->join('users','settings.user_id','=','users.id')
            ->get();
//        dd($users);
        foreach ($users as $u_data){
        }
//        $data = Auth::user()->id;
//        $data1 = DB::table('questions')->where('user_id',$data)->count();
//        $data2 = DB::table('answers')->where('user_id',$data)->count();

        $allInformation = DB::table('questions')->orderBy('id','DESC')->paginate(5);
//        dd($allInformation);
        $alldata =[
//            'data1'=> $data1,
//            'data2'=> $data2,
            'allInformation'=> $allInformation,
            'u_data'=>$u_data,
        ];
        return view('welcome',$alldata);
    }
}
