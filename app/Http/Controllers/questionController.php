<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class questionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function create()
    {
        return view('layouts.Questions.postquestion');
    }
    public function index()
    {
        $data = Auth::user()->id;
        $data1 = DB::table('questions')->where('user_id',$data)->count();
        $data2 = DB::table('answers')->where('user_id',$data)->count();

        $users_id = Auth::user()->id;
        $users=DB::table('questions')
            ->join('settings', 'questions.user_id','=','settings.user_id')
            ->join('users','settings.user_id','=','users.id')
            ->where('users.id','=',$users_id)
            ->get();
//        dd($users);
        foreach ($users as $u_data){
        }

        $allInformation = DB::table('questions')->where('user_id', $data)->orderBy('id','DESC')->paginate(2);
//        dd($allInformation);
        $alldata =[
            'data1'=> $data1,
            'data2'=> $data2,
            'allInformation'=> $allInformation,
            'u_data'=>$u_data,

        ];

        return view('layouts.Questions.questionlist',$alldata);
    }

    public function singleview($id)
    {
        DB::table('questions')
            ->where('id', $id)
            ->increment('view');
        $data = Question::find($id);
//        dd($data);
        $users_id = Auth::user()->id;
//        dd($users_id);
        $users=DB::table('questions')
        ->join('settings', 'questions.user_id','=','settings.user_id')
        ->join('users','settings.user_id','=','users.id')
        ->where('users.id','=',$users_id)
        ->get();
//        dd($users);
        foreach ($users as $u_data){
        }
        $data2 = Question::find($id)->User;
        $data1 =  DB::table('answers')->where('qns_id', $id)->get();
        $data3 =  DB::table('answers')->where('qns_id', $id)->count();
//        dd($data2);
        $value = [
            'data'=>$data,
            'ans'=>$data1,
            'user'=>$data2,
            'anscount'=>$data3,
            'u_data'=>$u_data,
        ];
        return view('layouts.Questions.singleview',$value);
    }
    public function store(Request $request)
    {
//        dd($request);
        $data = $request->all();
//        dd($data);
        $data['user_id']=Auth::user()->id;
        $this->validate($request, [
            'title'=>'required',
            'date' => 'required',
            'description' => 'required',
        ]);

        Question::create($data);
        Session::flash('message','Question Submitted successfully');
        return redirect()->back();
    }
    public function edit($id)
    {
        $data = Question::find($id);
        return view('layouts.Questions.edit',compact('data'));
    }
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $mydata= Question::find($id);
        $mydata->update($data);
        Session::flash('message','Update Successfully');
        return redirect()->back();
    }
    public function destroy($id)
    {
        $data = Question::find($id);
        $data->delete();
        Session::flash('message','Deleted Successfully');
        return redirect()->back();
    }

}
