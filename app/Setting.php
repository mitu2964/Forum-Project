<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable=['id','name','date','description','contact','hobby','img','user_id'];
    public function User()
    {
        return $this->belongsTo(User::class);
    }
    public function questions()
    {
        return $this->belongsTo(Question::class);
    }


}
