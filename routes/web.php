<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::get('/login', function () {
    return view('layouts.Users.login');
});
Route::get('/registration', function () {
    return view('layouts.Users.registration');
});
Route::get('/settings', function () {
    return view('layouts.Users.settings');
});
Route::get('/questions', function () {
    return view('layouts.Questions.questionlist');
});
Route::get('/post-questions', function () {
    return view('layouts.Questions.postquestion');
});
Route::get('/login', function () {
    return view('layouts.includes.login');
});


Route::get('/admin','DashboardController@index');
Route::get('/dashboard','DashboardController@questionlogin');
Route::get('/','WelcomeController@question');

Route::get('/settings/profile/{id}/create','settingController@edit');
//Route::get('/admin/about/{id}/profile','profileController@edit');
//Route::post('/admin/about/{id}/update','profileController@update');
Route::post('/settings/profile/{id}/update','settingController@update');


Route::get('/post-questions','questionController@create');
Route::post('/post-questions/store','questionController@store');
Route::get('/post-questions/questionlist','questionController@index');
Route::get('/post-questions/{id}/single-view','questionController@singleview');
Route::get('/post-questions/{id}/edit','questionController@edit');
Route::post('/post-questions/{id}/update','questionController@update');
Route::get('/post-questions/{id}/destroy','questionController@destroy');



//Route::get('/admin/questions/create','questionController@create');

Route::get('/admin/questions/questionlist','questionController@index');
Route::post('/admin/question/store','questionController@store');
Route::get('/admin/questions/{id}/edit','questionController@edit');
Route::post('/admin/questions/{id}/update','questionController@update');
Route::get('/admin/questions/{id}/destroy','questionController@destroy');
//Route::get('/search','questionController@search');


Route::post('/post-answer/store','answerController@store');
Route::get('/post-ans/{id}/edit','answerController@edit');
Route::post('/post-ans/{id}/update','answerController@update');
Route::get('/post-ans/{id}/destroy','answerController@destroy');

Route::get('/contact','contactController@index');
Route::post('/contact','contactController@contact');


//Route::get('/contact', function () {
//    return view('layouts.includes.contact');
//});
//Route::get('/admin/answers/{id}/edit','answerController@edit');
//Route::post('/admin/answers/{id}/update','answerController@update');
//Route::get('/admin/answers/{id}/destroy','answerController@destroy');
//Route::get('/admin/answers/create','answerController@create');
//Route::get('/admin/answers/questionlist','answerController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
