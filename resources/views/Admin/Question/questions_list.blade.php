
@extends('Admin.layouts.master')
@section('qns_list', 'active')
@section('content')

<!-- Content area -->
<div class="content">
    <div class="row">
        <div class="col-md-12">

            <!-- Basic layout-->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h2 class="panel-title">Questions Table
                        <span class="label label-success position-right" style="font-size: 14px">
                            @if(Session::has('message'))
                                {{Session::get('message')}}
                            @endif
                        </span>
                    </h2>
                    <div class="heading-elements">
                        <span class="label label-primary heading-text"><a href="{{url('/admin/questions/create')}}" style="color: white;font-size: 14px" >Add New Question</a></span>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Date</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allInformation as $question)
                            <tr>
                                <td>{{$question->id}}</td>
                                <td>{{$question->title}}</td>
                                <td>{{$question->date}}</td>
                                <td>{{$question->description}}</td>
                                <td>
                                    <a href="{{url('/admin/questions/'.$question->id.'/update')}}" class="btn btn-success">Edit</a>
                                    <a href="{{url('/admin/questions/'.$question->id.'/destroy')}}" onclick="return confirm('Do You Want To Delete?')" class="btn btn-danger">DELETE</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            {{--{{$allservice->Links()}}--}}

        </div>
    </div>
</div>

@endsection