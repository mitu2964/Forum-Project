@include('Admin.layouts.include.head')

<body>
@include('Admin.layouts.include.headermenu')
<div class="page-container">
    <div class="page-content">
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">
              @include('Admin.layouts.include.sidebarmenu')
            </div>
        </div>
        <div class="content-wrapper">
            <div class="page-header">
                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="{{'/admin'}}"><i class="icon-home2 position-left"></i> Home</a></li>
                        <li class="active">Dashboard / </li>
                        @yield('bread_crumbs')
                    </ul>
                    <ul class="breadcrumb-elements">
                        <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-gear position-left"></i>
                                Settings
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
                                <li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
                                <li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="icon-gear"></i> All settings</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            @yield('content')
            @include('Admin.layouts.include.footer')
        </div>
    </div>
</div>
</body>
</html>
