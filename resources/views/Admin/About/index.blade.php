@extends('Admin.layouts.master')
@section('show_about', 'active')
@section('content')

    <div class="content">

        <div class="panel panel-flat">

            <div class="panel-heading">
                <h2 class="panel-title text-center" style="border-bottom: 1px solid #DDDDDD; margin-bottom: 10px; margin-right: 10px">About Me<span class="label label-success position-right" style="font-size: 14px">
                    @if(Session::has('message'))
                            {{Session::get('message')}}
                        @endif
                </span>
                </h2>
                <div class="heading-elements">
                    <span class="label label-primary heading-text"><a href="{{url('admin/about/create')}}" style="color: white;font-size: 14px" >Update AboutMe</a></span>
                </div>
            </div>

            <div class="panel-body">
                <table class="table table-bordered">
                    <tr>
                        <th>Name</th>
                        <th>Date Of Birth</th>
                        <th>Description</th>
                        <th>Contact</th>
                        <th>Hobby</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                    @foreach($allInformation as $item)
                    <tr>
                        <td>{{$item->name}}</td>
                        <td>{{$item->date}}</td>
                        <td>{{$item->description}}</td>
                        <td>{{$item->contact}}</td>
                        <td>{{$item->hobby}}</td>
                        <td>{{$item->img}}</td>
                        <td>
                            <button><a>View</a></button>
                            <button><a href="{{url('/admin/about/'.$item->id.'/edit')}}">Edit</a></button>
                            {!! Form::open(['url'=>'/admin/about/'.$item->id.'/delete']) !!}
                            {!! Form::submit('Delete') !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection