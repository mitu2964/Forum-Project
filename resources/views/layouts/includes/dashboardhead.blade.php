
<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from templates.scriptsbundle.com/knowledge/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 12 Apr 2017 19:12:33 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="ScriptsBundle">
    <title>Knowledge Q&A Dashboard Template</title>
    <!-- =-=-=-=-=-=-= Favicons Icon =-=-=-=-=-=-= -->
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon" />
    <!-- =-=-=-=-=-=-= Mobile Specific =-=-=-=-=-=-= -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <!-- =-=-=-=-=-=-= Bootstrap CSS Style =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <!-- =-=-=-=-=-=-= Template CSS Style =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- =-=-=-=-=-=-= Font Awesome =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <!-- =-=-=-=-=-=-= Et Line Fonts =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('css/et-line-fonts.css')}}">
    <!-- =-=-=-=-=-=-= Google Fonts =-=-=-=-=-=-= -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic|Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css">
    <!-- =-=-=-=-=-=-= Owl carousel =-=-=-=-=-=-= -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.style.css')}}">
    <!-- =-=-=-=-=-=-= Highliter Css =-=-=-=-=-=-= -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/styles/shCoreDefault.css')}}" />
    <!-- =-=-=-=-=-=-= Css Animation =-=-=-=-=-=-= -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/animate.min.css')}}" />
    <!-- =-=-=-=-=-=-= Hover Dropdown =-=-=-=-=-=-= -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap-dropdownhover.min.css')}}" />
    <!-- JavaScripts -->
    <script src="{{asset('js/modernizr.js')}}"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- =-=-=-=-=-=-= HEADER =-=-=-=-=-=-= -->
<div class="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-4">
                <ul class="top-nav nav-left">
                    <li><a href="{{url('/')}}">Home</a>
                    <li  class="hidden-xs"><a href="{{url('/contact')}}">Contact Us</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-8">
                <ul class="top-nav nav-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" data-animations="fadeInUp">

                            {{--img--}}

                            <span class="hidden-xs small-padding">
								<span>{{ isset(Auth::user()->name) ? Auth::user()->name : 'NULL' }}</span>
							 <i class="fa fa-caret-down"></i>
							</span>
                        </a>
                        <ul class="dropdown-menu ">
                            <li><a href="{{url('/settings/profile/'.Auth::user()->id.'/create')}}"><i class=" icon-gears"></i> Profile Setting</a></li>
                            <li><a href="{{url('/post-questions/questionlist')}}"><i class="icon-heart"></i> Questions</a></li>
                            <li>
                                <a href="{{ url('/logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="navbar navbar-default">
    <div class="container">
        <!-- header -->
        <div class="navbar-header">
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- logo -->
            <a href="{{url('/')}}" class="navbar-brand"><img class="img-responsive" alt="" src="{{asset('images/logo.png')}}">
            </a>
            <!-- header end -->
        </div>
        <!-- navigation menu -->
        <div class="navbar-collapse collapse">
            <!-- right bar -->
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden-sm"><a href="how-work.html">How  It Works</a>
                </li>
                <li><a href="listing.html">Browse Questions</a>
                </li>
                <li class="dropdown"> <a class="dropdown-toggle " data-hover="dropdown" data-toggle="dropdown" data-animations="fadeInUp">Pages <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="index.html">Home </a>
                        </li>
                        <li><a href="index2.html">Home 2</a>
                        </li>
                        <li><a href="question-detial.html">Question Detail</a>
                        </li>
                        <li><a href="blog.html">Blog</a>
                        </li>
                        <li><a href="blog-1.html">Blog RightSide Bar</a>
                        </li>
                        <li><a href="blog-2.html">Blog LeftSide Bar</a>
                        </li>
                        <li><a href="blog-detial.html">Blog Single</a>
                        </li>
                        <li><a href="404.html">Error Page</a>
                        </li>
                        <li><a href="contact.html">Contact Us</a>
                        </li>
                        <li><a href="contact-2.html">Contact With Map</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <div class="btn-nav"><a href="{{url('/post-questions')}}" class="btn btn-primary btn-small navbar-btn">Post Question</a>
                    </div>
                </li>
            </ul>
        </div>
        <!-- navigation menu end -->
        <!--/.navbar-collapse -->
    </div>
</div>
