@include('layouts.includes.dashboardhead')

<!-- =-=-=-=-=-=-= User Profile =-=-=-=-=-=-= -->
<section id="user-profile" class="user-profile parallex">
    <div class="container">
        <!-- Row -->
        <div class="row">

            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="profile-sec ">
                    <div class="profile-head">
                        <div class="col-md-6 col-xs-12 col-sm-6 no-padding">
                            <div class="profile-avatar">
                                @if(isset($u_data->img))
                                <span><img class="img-responsive img-circle" alt="" src="{{asset('assets/upload_image/'.$u_data->img)}}"></span>
                                @endif
                                    <div class="profile-name">
                                    <h3>User Profile</h3>
                                    <i>Creative Graphic Designer</i>
                                    <ul class="social-btns">
                                        <li><a href="#" title=""><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li><a href="#" title=""><i class="fa fa-google-plus"></i></a>
                                        </li>
                                        <li><a href="#" title=""><i class="fa fa-twitter"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-6 no-padding">
                            <ul class="profile-count">
                                <li>171<i>Followers</i>
                                </li>
                                <li>13,725<i>Experience</i>
                                </li>
                                <li>120<i>Questions</i>
                                </li>
                            </ul>
                            <ul class="profile-connect">
                                <li><a title="" href="#">Follow</a>
                                </li>
                                <li><a title="" href="#">Hire Me</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Profile Sec -->
            </div>

        </div>
        <!-- Row End -->
    </div>
    <!-- end container -->
</section>

<!-- =-=-=-=-=-=-= User Profile end =-=-=-=-=-=-= -->

<!-- =-=-=-=-=-=-= User History =-=-=-=-=-=-= -->

<section class="section-padding-80 white">

    <div class="container">

        <!-- Row -->
        <div class="row">

            <div class="col-md-12 col-xs-12 col-sm-12">
                <!-- Question Listing -->
                    <div class="listing-grid">

                        <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-12 hidden-xs">
                                <a data-toggle="tooltip" data-placement="bottom" data-original-title="Martina Jaz" href="#">
                                    @if(isset($u_data))
                                    <img alt="" class="img-responsive center-block" src="{{asset('assets/upload_image/'.$u_data->img)}}">
                                    @endif
                                </a>
                            </div>
                            <div class="col-md-7 col-sm-8  col-xs-12">
                                <h3><a  href="#">{{$data->title}}</a></h3>
                                <div class="listing-meta"> <span><i class="fa fa-clock-o" aria-hidden="true"></i>{{ $data->created_at }}</span>  <span><i class="fa fa fa-eye" aria-hidden="true"></i>{{$data->view}} Views | Answers {{$anscount}}</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-2 col-xs-12">
                                <ul class="question-statistic">
                                    <li> <a href="{{url('/post-questions/'.$data->id.'/edit')}}" data-toggle="tooltip" data-placement="bottom" data-original-title="Answers"><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a>
                                    </li>
                                    <li> <a  href="{{url('/post-questions/'.$data->id.'/destroy')}}" onclick="return confirm('Do You Want To Delete?')" data-toggle="tooltip" data-placement="bottom" data-original-title="Votes">
                                            <span><i class="fa fa-trash-o" aria-hidden="true"></i></span></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-10 col-sm-10  col-xs-12">
                                <p>{{$data->description}}</p>
                            </div>
                        </div>
                    </div>
                @foreach($ans as $item)
                    <div class="listing-grid" style="margin-left: 100px">

                        <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-12 hidden-xs">
                                <a data-toggle="tooltip" data-placement="bottom" data-original-title="Martina Jaz" href="#">
                                    @if(isset($u_data))
                                    <img alt="User Image" class="img-responsive center-block"  src="{{asset('assets/upload_image/'.$u_data->img)}}">
                                        @endif
                                </a>
                            </div>
                            <div class="col-md-7 col-sm-8  col-xs-12">
                                <h3><a  href="#">{{$item->title}}</a></h3>
                                <div class="listing-meta"> <span><i class="fa fa-clock-o" aria-hidden="true"></i>{{ $item->created_at }}</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-2 col-xs-12">
                                <ul class="question-statistic">
                                    <li> <a href="{{url('/post-ans/'.$item->id.'/edit')}}" data-toggle="tooltip" data-placement="bottom" data-original-title="Answers"><span><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></a>
                                    </li>
                                    <li> <a  href="{{url('/post-ans/'.$item->id.'/destroy')}}" onclick="return confirm('Do You Want To Delete?')" data-toggle="tooltip" data-placement="bottom" data-original-title="Votes">
                                            <span><i class="fa fa-trash-o" aria-hidden="true"></i></span></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-10 col-sm-10  col-xs-12">
                                <p>{{$item->description}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="clearfix" style="margin-left: 280px">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-8 ">

                                <div class="box-panel">

                                    <h2>Post Your Answer
                                        <button class="btn btn-success">
                                            @if(Session::has('message'))
                                                {{Session::get('message')}}
                                            @endif
                                        </button>
                                    </h2>
                                    <hr>
                                    <!-- form login -->
                                    <form class="margin-top-40" role="form" method="POST" action="{{ url('/post-answer/store') }}">
                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                            <label for="title">Answer Title</label>
                                            <input type="text" class="form-control" name="title" required autofocus placeholder="Ex: Bootstrap not working"/>

                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                                            <label for="date">Date</label>

                                            <input type="date" class="form-control" name="date" required>

                                            @if ($errors->has('date'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                            <label for="description">Answer Details</label>

                                            <textarea cols="12" rows="12" placeholder="Post Your Answer Details Here....." name="description" class="form-control"></textarea>

                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                        <input type="hidden" name="qns_id" value="{{$data->id}}" >
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary pull-right">
                                                Publish Your Answer
                                            </button>
                                        </div>
                                    </form>
                                    <!-- form login -->

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
</section>
<!-- =-=-=-=-=-=-= Main Area End =-=-=-=-=-=-= -->


<!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
@include('layouts.includes.footer')