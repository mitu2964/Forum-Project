@include('layouts.includes.dashboardhead')



    <!-- =-=-=-=-=-=-= User Profile =-=-=-=-=-=-= -->
    <section id="user-profile" class="user-profile parallex">
        <div class="container">
            <!-- Row -->
            <div class="row">

                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="profile-sec ">
                        <div class="profile-head">
                            <div class="col-md-6 col-xs-12 col-sm-6 no-padding">
                                <div class="profile-avatar">
                                    <span><img class="img-responsive img-circle" alt="" src="{{asset('assets/upload_image/'.$data->img)}}"></span>
                                    <div class="profile-name">
                                        <h3>User Profile</h3>
                                        <i>Creative Graphic Designer</i>
                                        <ul class="social-btns">
                                            <li><a href="#" title=""><i class="fa fa-facebook"></i></a>
                                            </li>
                                            <li><a href="#" title=""><i class="fa fa-google-plus"></i></a>
                                            </li>
                                            <li><a href="#" title=""><i class="fa fa-twitter"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6 no-padding">
                                <ul class="profile-count">
                                    <li>171<i>Followers</i>
                                    </li>
                                    <li>13,725<i>Experience</i>
                                    </li>
                                    <li>120<i>Questions</i>
                                    </li>
                                </ul>
                                <ul class="profile-connect">
                                    <li><a title="" href="#">Follow</a>
                                    </li>
                                    <li><a title="" href="#">Hire Me</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Profile Sec -->
                </div>

            </div>
            <!-- Row End -->
        </div>
        <!-- end container -->
    </section>

    <!-- =-=-=-=-=-=-= User Profile end =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= User Form Settings =-=-=-=-=-=-= -->
    <section class="section-padding-80" id="login">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4">

                    <div class="box-panel">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form enctype="multipart/form-data" class="form-horizontal" role="form" method="POST" action="{{ url('/settings/profile/'.$data['id'].'/update') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Upload Image</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            Browse… <input type="file" name="img">
                                        </span>
                                    </span>
                                    <input type="text" name="img" class="form-control" readonly>
                                </div>
                                <img src="{{asset('assets/upload_image/'.$data->img)}}" alt="Current image" width="350" height="350" />
                            </div>

                            <button type="submit" class="btn btn-primary btn-lg">Change Image</button>

                        </form>

                    </div>
                </div>

                <div class="col-md-8">

                    <div class="box-panel">

                        <!-- form login -->
                        {!! Form::model($data,['url' => '/settings/profile/'.$data['id'].'/update', 'class' =>'form-horizontal','files' => true   ]) !!}


                        <div class="form-group">
                            {!! Form::label('name','Full Name') !!}
                            {!! Form::text('name', null,
                                array('class'=>'form-control',
                                      'placeholder'=>'Your Full name')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('date','Date of Birth') !!}
                            {!! Form::date('date', null,
                                array('class'=>'form-control',
                                      'placeholder'=>'Your Date of birth')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('description','Description') !!}
                            {!! Form::textarea('description', null,
                                array('class'=>'form-control',
                                      'placeholder'=>'Your Description')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('contact','Contact') !!}
                            {!! Form::text('contact', null,
                                array('class'=>'form-control',
                                      'placeholder'=>'Your Contact Number')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('hobby','Your Hobby') !!}
                            {!! Form::text('hobby', null,
                                array('class'=>'form-control',
                                      'placeholder'=>'Your Hobby')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Update',
                              array('class'=>'btn btn-primary')) !!}
                        </div>
                        {!! Form::close() !!}
                        <!-- form login -->

                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    <!-- =-=-=-=-=-=-= User Form Settings End =-=-=-=-=-=-= -->

</div>
    </section>

@include('layouts.includes.footer')
